var webpack = require('webpack');
var path = require('path');

// Naming and path settings
var appName = 'app';
var entryPoint = './src/main.js';
var exportPath = path.resolve(__dirname, './build/static/');

// Enviroment flag
var plugins = [];
var env = process.env.WEBPACK_ENV;

// Differ settings based on production flag
if (env === 'production') {
  var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;

  plugins.push(new UglifyJsPlugin({ minimize: true }));
  plugins.push(new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }
  ));

  appName = appName + '.min.js';
} else {
  appName = appName + '.js';
}

plugins.push(new webpack.ProvidePlugin({
    '$': "jquery",
    'jQuery': "jquery",
    "window.jQuery": "jquery",
    "window.Tether": 'tether',
    'Popper': 'popper.js'
}));

// Main Settings config
module.exports = {
  entry: entryPoint,
  output: {
    path: exportPath,
    publicPath: '/static/',
    filename: appName
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    modules: ["node_modules", path.join(__dirname, '../node_modules')],
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      'src': path.resolve(__dirname, '../src'),
      'assets': path.resolve(__dirname, '../src/assets'),
      'components': path.resolve(__dirname, '../src/components'),
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader', // <style lang="scss">
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax' // <style lang="sass">
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                path.resolve(__dirname, './src/scss/_variables.scss')
              ]
            },
          },
        ],
      },
      {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader'
         ]
      },
      {
        test:/\.(jade|pug)$/,
        exclude: ['/node_modules/'],
        loader: 'pug-html-loader?pretty',
        options: {
          pretty: (env === 'production')
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins
};