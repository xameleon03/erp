
require('./conf/init.js');

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Router from 'vue-router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

const App = Vue.component('App',require('./components/App.vue').default);
const Main = Vue.component('Main',require('./components/Main.vue').default);
const Products = Vue.component('Products',require('./components/Products.vue').default);
const Movement = Vue.component('Movement',require('./components/ProductsMovement.vue').default);
const Tranzaction = Vue.component('Tranzaction',require('./components/Tranzactions.vue').default);
const Report = Vue.component('Report',require('./components/Reports.vue').default);
const Contragent = Vue.component('Contragent',require('./components/Contragents.vue').default);
const Store = Vue.component('Store',require('./components/Stores.vue').default);

const routes = [
	{path: '/', component: Main},
	{path: '/products/', component: Products},
	{path: '/movement/', component: Movement},
	{path: '/tranzactions/', component: Tranzaction},
	{path: '/reports/', component: Report},
	{path: '/contragents/', component: Contragent},
	{path: '/stores',component: Store},
];

Vue.use(BootstrapVue);
Vue.use(Router);

var router = new Router({
	mode: 'history',
	routes,
	history: true,
});

const app = new Vue({
	router,
	render: h => h(App)
}).$mount('#app');